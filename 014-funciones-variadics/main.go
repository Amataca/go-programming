package main

import "fmt"

func sumar(numeros ...int) int {
	//el total inicial es 0
	total := 0
	//recorrer todos los numeros
	for _, numero := range numeros {
		//en cada iteracion sumar al total el valor del numero
		total = numero + total
	}
	//retornar el valor total
	return total
}

func main() {
	fmt.Println(sumar(2))
	fmt.Println(sumar(2, 2))
	fmt.Println(sumar(5, 4, 3))
	fmt.Println(sumar(5, 5, 5, 5))
}
