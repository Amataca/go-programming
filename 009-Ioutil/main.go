package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	hello := []byte("Hello world!")

	// escribe 'hello, world' a test.txt que puede ser leido/escrito por usuario y leido por otros
	err := ioutil.WriteFile("test.txt", hello, 0644)

	if err != nil {
		panic(err)
	}

	//lee test.txt

	data, err := ioutil.ReadFile("test.txt")
	if err != nil {
		panic(err)
	}

	//Mostrando salida: 'the file contains: Hello, world!

	fmt.Println("The file contains: " + string(data))
}
