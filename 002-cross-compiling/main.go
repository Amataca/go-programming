package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("sistema operativo: %v\n Arquitectura: %v\n", runtime.GOOS, runtime.GOARCH)
}
