package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	//Accedemos al cliente mediante http.Client
	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	//creamos un nuevo request tipo POST mediante http.NewRequest
	request, err := http.NewRequest("POST", "https://httpbin.org/post", nil)

	if err != nil {
		log.Fatal(err)
	}
	//Ahora ejecutamos el request mediante el client
	response, err := client.Do(request)
	//Leemos el contenido de la respuesta, y cerramos body de dicha respuesta una ves que se complete la funcion main
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Fatal(err)
	}

	//Imprimimos el contenido del Body
	fmt.Printf("%s", body)

}
