package main

import (
	"fmt"
	"time"
)

//simulacion de api
func leerApí() string {
	time.Sleep(5 * time.Second)
	return "respuesta del api"
}

func main() {
	c1 := make(chan string, 1)

	go func() {
		c1 <- leerApí()
	}()

	select {
	case res := <-c1:
		fmt.Println(res)
	case <-time.After(2 * time.Second):
		fmt.Println("timeout")
	}
}
