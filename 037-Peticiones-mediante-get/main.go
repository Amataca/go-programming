package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	res, err := http.Get("https://httpbin.org/")

	if err != nil {
		log.Fatal(err)
	}
	//cuando los datos an sido leidos por el cliente la conexion se cierra
	defer res.Body.Close()
	//Ahora leemos el contenido de Res.Body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", body)

}
