package main

import "fmt"

func main() {

	vehiculo1 := "rojo"
	fmt.Println("El vehiculo1 es", vehiculo1)

	//copia, asigna nuevo valor en memoria
	vehiculo2 := vehiculo1
	fmt.Println("El vehiculo2 es", vehiculo2)

	//referencia en go
	vehiculo3 := &vehiculo1
	fmt.Println("El vehiculo3 es", *vehiculo3)

	vehiculo1 = "gris"

	fmt.Println("El vehiculo1 es", vehiculo1, &vehiculo1)
	fmt.Println("El vehiculo2 es", vehiculo2, &vehiculo2)
	fmt.Println("El vehiculo3 es", *vehiculo3, vehiculo3)

}
