package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	files, err := ioutil.ReadDir("./")
	if err != nil {
		panic(err)
	}

	fmt.Println("Files and Folders in the current directory:")

	for _, fileInfo := range files {
		fmt.Println(fileInfo.Name())
	}
}
