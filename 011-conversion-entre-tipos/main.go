package main

import (
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	//De string a booleno
	var mayorDeEdad string = "true"
	boolVal, _ := strconv.ParseBool(mayorDeEdad)
	fmt.Println(boolVal, reflect.TypeOf(boolVal))

	//Otro ejemplo, de booleano a string
	var menorDeEdad bool = true
	strVal := strconv.FormatBool(menorDeEdad)
	fmt.Println(strVal, reflect.TypeOf(strVal))
}
