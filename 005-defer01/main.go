package main

import "fmt"

func main() {
	fmt.Println("First main statement")
	defer logExit("main") // position of defer statement here does not matter
	fmt.Println("Last main statement")
}

func logExit(name string) {
	fmt.Printf("Function %s returned\n", name)
}
