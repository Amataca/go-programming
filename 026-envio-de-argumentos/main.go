package main

import (
	"fmt"
	"os"
)

func main() {
	//Metodo os.Args permite iterar mediante range a los argumentos de la
	//linea de comandos comenzando con el nombre del programa
	for k, v := range os.Args {
		fmt.Printf("Argumento %v: %v\n", k, v)
	}

	// Para invocar el script:
	// go build main.go
	// ./main argumento Hola Mundo
}
