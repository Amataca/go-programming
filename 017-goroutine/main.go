package main

import (
	"fmt"
	"time"
)

func leerArchivo() string {
	time.Sleep(time.Second * 5)
	return "Datos del archivo"
}

func main() {
	go func() {
		Datos := leerArchivo()
		fmt.Println(Datos)
	}()

	fmt.Println("Continuar con la ejecucion")
}
