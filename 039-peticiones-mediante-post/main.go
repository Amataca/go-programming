package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func main() {
	//realizar composicion de datos
	datos := strings.NewReader(`{"nombre":"Carlos"}`)
	//realizo el envío de los datos mediante hhtp.Post
	respuesta, err := http.Post("https://httpbin.org/post", "application/json", datos)

	if err != nil {
		log.Fatal(err)
	}
	//Cerramos la lectura del body al completar el programa
	defer respuesta.Body.Close()
	//Realizamos la lectura del body
	body, err := ioutil.ReadAll(respuesta.Body)

	if err != nil {
		log.Fatal(err)
	}
	//impresión del cuerpo
	fmt.Printf("%s", body)

}
