package main

import (
	"io"
	"log"
	"os"
)

func main() {
	//leer datos de origin
	origin, err := os.Open("origin.txt")
	if err != nil {
		log.Fatal(err)
	}
	//cierra el archivo "origin.txt" al terminar el programa
	defer origin.Close()
	//crear un nuevo archivo
	destino, err := os.OpenFile("destino.txt", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	// cierrra el archivo "destino.txt" al terminar el programa
	defer destino.Close()
	//Copiar datos
	resultadoCopia, err := io.Copy(destino, origin)
	if err != nil {
		log.Fatal(err)
	}

	_ = resultadoCopia
}
