package main

import "fmt"

type Pais struct {
	Nombre    string
	Capital   string
	Idioma    string
	Poblacion int
}

func main() {
	//Ejemplo sencillo de un struct
	irlanda := Pais{
		Nombre:    "Irlanda",
		Capital:   "Dublin",
		Idioma:    "Irlandes",
		Poblacion: 4857000,
	}
	//Instanciar Pais:

	var belice Pais

	belice.Nombre = "Belice"
	belice.Capital = "belmopan"
	belice.Idioma = "Ingles"
	belice.Poblacion = 398000

	//Acceder a propiedades

	panama := Pais{
		Nombre:    "Panama",
		Capital:   "Ciudad de Panama",
		Idioma:    "Español",
		Poblacion: 4170607,
	}
	fmt.Println("Nombre: ", panama.Nombre)
	fmt.Println("Capital: ", panama.Capital)
	fmt.Println("Idioma: ", panama.Idioma)
	fmt.Println("Población: ", panama.Poblacion)

	fmt.Println(irlanda)
	fmt.Println(belice)

	//Instanciar con constructor new

	colombia := new(Pais)
	fmt.Printf("%+v\n", colombia)
	colombia.Nombre = "Colombia"
	colombia.Capital = "Bogota"
	colombia.Idioma = "Español"
	colombia.Poblacion = 49e6
	fmt.Printf("%+v\n", colombia)
}
