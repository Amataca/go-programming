package main

import "fmt"

func equivalenciaEnPies(altura float32) float32 {
	altura = altura * 3.28
	return altura
}

func conversionAlEnvejecer(altura *float32) float32 {
	*altura = *altura - 0.10
	return *altura
}

func main() {
	var altura float32 = 1.70
	//Resultado normal
	fmt.Println("La altura es:", altura, "mts")
	fmt.Println("La altura es:", equivalenciaEnPies(altura), " pies")
	fmt.Println("La nueva altura es:", altura, "mts")

	//Resultado con puntero

	fmt.Println("Al envejecer:", conversionAlEnvejecer(&altura), "mts")
	fmt.Println("Despues de envejecer:", altura, "mts") //Variable altura cambia despuez de modificarse en la función
}
