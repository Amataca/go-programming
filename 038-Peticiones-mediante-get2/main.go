package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	//Crear cliente http:
	clienteHttp := &http.Client{}
	// Si quieres agregar parámetros a la URL simplemente haz una
	// concatenación :)
	url := "https://httpbin.org/get"
	// creacion de peticion, la peticion recibe 3 argumentos, el tipo o verbo, la url, y los datos.. Si es GET o simplemente no queremos enviar datos pasamos nil.
	peticion, err := http.NewRequest("GET", url, nil)

	if err != nil {
		// Maneja el error de acuerdo a tu situación
		log.Fatalf("Error creando peticion:%v", err)
	}
	//Podemos agregar encabezados
	peticion.Header.Add("X-Hola-Mundo", "Ejemplos")

	//hacer la peticion
	respuesta, err := clienteHttp.Do(peticion)

	//En caso de que err sea nil debemos cerrar el cuerpo, podemos usar defer para cerrarlo al final:
	defer respuesta.Body.Close()

	//Se accede al cuerpo
	cuerpoRespuesta, err := ioutil.ReadAll(respuesta.Body)

	if err != nil {
		log.Fatalf("Error leyendo respuesta: %v", err)
	}
	//convertido de byte a string
	respuestaString := string(cuerpoRespuesta)
	//Imprimir codigo de respuesta con StatusCode
	log.Printf("Codigo de respuesta: %d", respuesta.StatusCode)

	contentType := respuesta.Header.Get("Content-type")
	log.Printf("El tipo de contenido: '%s'", contentType)
	log.Printf("Cuerpo de respuesta del servidor: '%s'", respuestaString)
}
