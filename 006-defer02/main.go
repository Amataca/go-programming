package main

import "fmt"

func main() {
	defer logNum(1)
	fmt.Println("First main statement")
	defer logNum(2)
	defer logNum(3)

	fmt.Println("Last main statement")
	defer logNum(4)
}
func logNum(i int) {
	fmt.Printf("Num %d\n", i)
}
