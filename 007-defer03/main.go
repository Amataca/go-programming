package main

import "fmt"

func main() {
	i := 1
	defer logNum(i)
	fmt.Println("First main statement")
	i++
	defer logNum(i)
	defer logNum(i * i)

	return
}

func logNum(i int) {
	fmt.Printf("Num %d\n", i)
}
