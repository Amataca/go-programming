package main

import "fmt"

func main() {
	fmt.Println(plusOne(1))
	return
}

func plusOne(i int) (result int) { //exagerado! solo para demostracion
	defer func() { //funcion anonima, debe ser llamada por añadir()
		result += 1
		result *= result
	}()

	//i es devuelto como result, Se actualiza con la función diferida anterior
	return i
}
