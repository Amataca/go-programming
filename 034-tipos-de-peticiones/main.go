package main

import "net/http"

func Home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	switch r.Method {
	case "GET":
		w.Write([]byte("Se realizó una peticion GET"))

	case "POST":
		w.Write([]byte("Se realizó una peticion POST"))

	case "PUT":
		w.Write([]byte("Se realizo una peticion PUT"))

	case "DELETE":
		w.Write([]byte("Se realizo una peticion DELETE"))

	default:
		w.Write([]byte("Se realizo una peticion " + r.Method))
	}
}

func main() {

	http.HandleFunc("/", Home)
	http.ListenAndServe(":8000", nil)
}
