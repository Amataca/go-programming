package main

import (
	"fmt"
	"time"
)

func main() {
	tdr := time.Tick(3 * time.Second)

	for horaActual := range tdr {
		fmt.Println("La hora es", horaActual)
	}
}
