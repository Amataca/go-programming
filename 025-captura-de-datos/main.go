package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	//para capturar datos desde la linea de comando podemos utilizar la libreria bufio
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Ingresa tu nombre: ")
	//captura de valores en variables.En este caso en nombre
	//Con ReadString se debe espicificar el limitador en forma de byte, en este caso una nueva linea y por ello se usan comillas simples,
	//esta instruccion tambien pone en pausa el programa hasta que se ingresa un string
	nombre, _ := reader.ReadString('\n')

	//Instruccion adicional, Revisar documentacion
	nombre = strings.Replace(nombre, "\n", "", -1)

	fmt.Println("-------------")

	fmt.Println("Nombre:", nombre)

	fmt.Println("-------------")
}
